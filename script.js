const board = document.getElementById('board') 

const colors = [
    '#FF0000',
    '#00FFFF',
    '#7FFFD4',
    '#00BFFF',
    '#C0C0C0',
    '#778899',
    '#FFFF00',
    '#228B22'
]

const SQUARES_NUMBER = 500

for(let i = 0; i < SQUARES_NUMBER; i++){
    const square = document.createElement('div')
    square.classList.add('square')

    square.addEventListener('mouseover', () => {
        setColor(square)
    })

    square.addEventListener('mouseleave', () => {
        removeColor(square)
    })

    board.append(square);
}

function setColor(element){
    console.log('over')
    const color = getRandomColor()
    const color2 = getRandomColor()
    element.style.background = color
    element.style.border = `2px solid ${color2}`
    element.style.width = '20px'
}

function removeColor(element){
    console.log('revo')
    element.style.background = colors[7]
    element.style.border = 'none'
    element.style.width = '16px'
}

function getRandomColor(){
    const colorRandom = Math.floor(Math.random() * colors.length)
    return colors[colorRandom]
}